import React, { useState } from "react";
import { Modal, Button } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import { Employee } from "./EmployeeTable";

interface DeleteEmployeeModalProps {
  employee: Employee;
  deleteEmployee: (id: string) => void;
}

const DeleteEmployeeModal = ({
  employee,
  deleteEmployee,
}: DeleteEmployeeModalProps) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalEmployee, setModalEmployee] = useState<Employee>(employee);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setModalEmployee(employee);
    setIsModalVisible(false);
  };

  const handleOk = () => {
    deleteEmployee(employee.id);
    setIsModalVisible(false);
  };

  return (
    <>
      <Button type="link" onClick={showModal}>
        <DeleteOutlined />
      </Button>
      <Modal
        title={`Delete employee - ${employee.id}`}
        visible={isModalVisible}
        centered
        onCancel={handleCancel}
        onOk={handleOk}
      >
        <p>
          Employee <b>{employee.name}</b> with id <b>{employee.id}</b> will be
          deleted!
        </p>
      </Modal>
    </>
  );
};

export default DeleteEmployeeModal;
