import React, { useState } from "react";
import { Modal, Button, Input } from "antd";
import { EditOutlined } from "@ant-design/icons";
import { Employee } from "./EmployeeTable";

interface EditEmployeeModalProps {
  employee: Employee;
  editEmployee: (id: string, edited: Employee) => void;
}

const EditEmployeeModal = ({
  employee,
  editEmployee,
}: EditEmployeeModalProps) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalEmployee, setModalEmployee] = useState<Employee>(employee);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setModalEmployee(employee);
    setIsModalVisible(false);
  };

  const handleOk = () => {
    editEmployee(employee.id, modalEmployee);
    setIsModalVisible(false);
  };

  return (
    <>
      <Button type="link" onClick={showModal}>
        <EditOutlined />
      </Button>
      <Modal
        title={`Edit employee - ${employee.id}`}
        visible={isModalVisible}
        centered
        onCancel={handleCancel}
        onOk={handleOk}
      >
        <Input
          placeholder="Name"
          value={modalEmployee.name}
          type="text"
          onChange={(e) => {
            setModalEmployee({ ...modalEmployee, name: e.target.value });
          }}
        />
        <Input
          placeholder="Login"
          value={modalEmployee.login}
          type="text"
          onChange={(e) => {
            setModalEmployee({ ...modalEmployee, login: e.target.value });
          }}
        />
        <Input
          placeholder="Salary"
          value={modalEmployee.salary}
          type="number"
          onChange={(e) => {
            setModalEmployee({
              ...modalEmployee,
              salary: Number(e.target.value),
            });
          }}
        />
      </Modal>
    </>
  );
};

export default EditEmployeeModal;
