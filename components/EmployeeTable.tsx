import React, { useEffect, useState } from "react";
import { Table, Space, Button, Row, Col } from "antd";
import DeleteOutlined from "@ant-design/icons/lib/icons/DeleteOutlined";
import Input from "antd/lib/input/Input";
import { Breakpoint } from "antd/lib/_util/responsiveObserve";
import EditEmployeeModal from "./EditEmployeeModal";
import DeleteEmployeeModal from "./DeleteEmployeeModal";

export interface Employee {
  id: string;
  key: string;
  name: string;
  login: string;
  salary: number;
}

const data: Employee[] = [
  {
    id: "e0001",
    key: "e0001",
    login: "hpotter",
    name: "Harry Potter",
    salary: 2012.99,
  },
  {
    id: "e0002",
    key: "e0002",
    login: "rwesley",
    name: "Ron Weasley",
    salary: 19234.5,
  },
  {
    id: "e0003",
    key: "e0003",
    login: "ssnape",
    name: "Severus Snape",
    salary: 4000.0,
  },
  {
    id: "e0004",
    key: "e0004",
    login: "rhagrid",
    name: "Rubeus Hagrid",
    salary: 3999.999,
  },
  {
    id: "e0005",
    key: "e0005",
    login: "voldemort",
    name: "Lord Voldemort",
    salary: 523.4,
  },
];

export default function EmployeeTable() {
  const [employees, setEmployees] = useState<Employee[]>(data);
  const [minSalaryFilter, setMinSalaryFilter] = useState(0);
  const [maxSalaryFilter, setMaxSalaryFilter] = useState(999999999);

  const stringSorter = (field: keyof Employee, a: Employee, b: Employee) => {
    const fieldA = String(a[field]).toUpperCase();
    const fieldB = String(b[field]).toUpperCase();
    if (fieldA < fieldB) {
      return -1;
    }
    if (fieldA > fieldB) {
      return 1;
    }
    return 0;
  };

  useEffect(() => {
    const filteredData = data.filter(
      (employee) =>
        employee.salary >= minSalaryFilter && employee.salary <= maxSalaryFilter
    );
    console.log(maxSalaryFilter, minSalaryFilter, filteredData);
    setEmployees(filteredData);
  }, [minSalaryFilter, maxSalaryFilter]);

  const resetData = () => {
    setEmployees(data);
    setMinSalaryFilter(0);
    setMaxSalaryFilter(999999999);
  };

  const handleMinSalaryFilterChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setMinSalaryFilter(Number(e.target.value));
  };

  const handleMaxSalaryFilterChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setMaxSalaryFilter(Number(e.target.value));
  };

  const editEmployee = (id: string, employee: Employee) => {
    const copyEmployees: Employee[] = JSON.parse(JSON.stringify(employees));
    const filtered = copyEmployees.filter((v) => v.id !== id);
    filtered.push(employee);
    setEmployees(filtered);
  };

  const deleteEmployee = (id: string) => {
    const copyEmployees: Employee[] = JSON.parse(JSON.stringify(employees));
    const filtered = copyEmployees.filter((v) => v.id !== id);
    setEmployees(filtered);
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
      render: (text: string) => <a>{text}</a>,
      sorter: (a: Employee, b: Employee) => stringSorter("id", a, b),
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      sorter: (a: Employee, b: Employee) => stringSorter("name", a, b),
    },
    {
      title: "Login",
      dataIndex: "login",
      key: "login",
      sorter: (a: Employee, b: Employee) => stringSorter("login", a, b),
      responsive: ["md"] as Breakpoint[],
    },
    {
      title: "Salary",
      dataIndex: "salary",
      key: "salary",
      sorter: (a: Employee, b: Employee) => a.salary - b.salary,
      responsive: ["md"] as Breakpoint[],
    },
    {
      title: "Action",
      key: "action",
      render: (text: any, record: Employee) => (
        <Space size="middle">
          <EditEmployeeModal employee={record} editEmployee={editEmployee} />
          <DeleteEmployeeModal
            employee={record}
            deleteEmployee={deleteEmployee}
          />
        </Space>
      ),
    },
  ];

  return (
    <div>
      <Row>
        <Col style={{ padding: "10px" }}>
          <p>Minumum salary filter</p>
          <Input
            placeholder="Minimum Salary"
            value={minSalaryFilter}
            type="number"
            onChange={handleMinSalaryFilterChange}
          />
        </Col>
        <Col style={{ padding: "10px" }}>
          <p>Maximum salary filer</p>
          <Input
            placeholder="Maximum Salary"
            value={maxSalaryFilter}
            type="number"
            onChange={handleMaxSalaryFilterChange}
          />
        </Col>
        <Col style={{ padding: "10px" }}>
          <Button onClick={() => resetData()}>Reset data</Button>
        </Col>
      </Row>
      <Table columns={columns} dataSource={employees} />
    </div>
  );
}
