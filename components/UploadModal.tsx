import React, { useState } from "react";
import { Modal, Button, Upload, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { UploadChangeParam } from "antd/lib/upload";
import { RcFile, UploadFile, UploadProps } from "antd/lib/upload/interface";

const UploadModal = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const beforeUpload = (file: RcFile, FileList: RcFile[]) => {
    // https://datatracker.ietf.org/doc/html/rfc7111#section-5.1
    // The Internet media type [RFC6838] for a CSV document is text/csv.

    const isCsvFile = file.type === "text/csv";
    if (!isCsvFile) {
      message.error("You can only upload CSV file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("CSV file must be smaller than 2MB!");
    }
    return isCsvFile && isLt2M;
  };

  const uploadProps: UploadProps = {
    name: "file",
    action: "https://nphc-hr.free.beeceptor.com/users/upload",
    headers: {
      authorization: "authorization-text",
    },
    accept: ".csv",
    beforeUpload,
    onChange(info: UploadChangeParam<UploadFile<any>>) {
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Upload CSV
      </Button>
      <Modal
        title="Upload CSV"
        visible={isModalVisible}
        centered
        onCancel={handleCancel}
        footer={null}
      >
        <Upload {...uploadProps}>
          <Button icon={<UploadOutlined />}>Click to Upload</Button>
        </Upload>
      </Modal>
    </>
  );
};

export default UploadModal;
